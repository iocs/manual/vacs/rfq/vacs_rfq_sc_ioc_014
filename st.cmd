#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_tcp350
#
require vac_ctrl_tcp350,1.5.1


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_tcp350_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: RFQ-010:Vac-VEPT-01100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-01100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4001")

#
# Device: RFQ-010:Vac-VPT-01100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-01100, CONTROLLERNAME = RFQ-010:Vac-VEPT-01100")

#
# Device: RFQ-010:Vac-VEPT-02100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-02100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4002")

#
# Device: RFQ-010:Vac-VPT-02100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-02100, CONTROLLERNAME = RFQ-010:Vac-VEPT-02100")

#
# Device: RFQ-010:Vac-VEPT-03100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-03100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4003")

#
# Device: RFQ-010:Vac-VPT-03100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-03100, CONTROLLERNAME = RFQ-010:Vac-VEPT-03100")

#
# Device: RFQ-010:Vac-VEPT-04100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-04100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4004")

#
# Device: RFQ-010:Vac-VPT-04100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-04100, CONTROLLERNAME = RFQ-010:Vac-VEPT-04100")

#
# Device: RFQ-010:Vac-VEPT-05100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-05100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4005")

#
# Device: RFQ-010:Vac-VPT-05100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-05100, CONTROLLERNAME = RFQ-010:Vac-VEPT-05100")

#
# Device: RFQ-010:Vac-VEPT-06100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-06100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4006")

#
# Device: RFQ-010:Vac-VPT-06100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-06100, CONTROLLERNAME = RFQ-010:Vac-VEPT-06100")

#
# Device: RFQ-010:Vac-VEPT-07100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-07100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4007")

#
# Device: RFQ-010:Vac-VPT-07100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-07100, CONTROLLERNAME = RFQ-010:Vac-VEPT-07100")

#
# Device: RFQ-010:Vac-VEPT-08100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-08100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4008")

#
# Device: RFQ-010:Vac-VPT-08100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-08100, CONTROLLERNAME = RFQ-010:Vac-VEPT-08100")

#
# Device: RFQ-010:Vac-VEPT-09100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-09100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4009")

#
# Device: RFQ-010:Vac-VPT-09100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-09100, CONTROLLERNAME = RFQ-010:Vac-VEPT-09100")

#
# Device: RFQ-010:Vac-VEPT-0A100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEPT-0A100, IPADDR = moxa-vac-rfq-2.tn.esss.lu.se, PORT = 4010")

#
# Device: RFQ-010:Vac-VPT-0A100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = RFQ-010:Vac-VPT-0A100, CONTROLLERNAME = RFQ-010:Vac-VEPT-0A100")
