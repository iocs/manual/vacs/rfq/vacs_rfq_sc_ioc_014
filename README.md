# IOC for RFQ vacuum turbopumps

## Used modules

*   [vac_ctrl_tcp350](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_tcp350)


## Controlled devices

*   RFQ-010:Vac-VEPT-01100
    *   RFQ-010:Vac-VPT-01100
*   RFQ-010:Vac-VEPT-02100
    *   RFQ-010:Vac-VPT-02100
*   RFQ-010:Vac-VEPT-03100
    *   RFQ-010:Vac-VPT-03100
*   RFQ-010:Vac-VEPT-04100
    *   RFQ-010:Vac-VPT-04100
*   RFQ-010:Vac-VEPT-05100
    *   RFQ-010:Vac-VPT-05100
*   RFQ-010:Vac-VEPT-06100
    *   RFQ-010:Vac-VPT-06100
*   RFQ-010:Vac-VEPT-07100
    *   RFQ-010:Vac-VPT-07100
*   RFQ-010:Vac-VEPT-08100
    *   RFQ-010:Vac-VPT-08100
*   RFQ-010:Vac-VEPT-09100
    *   RFQ-010:Vac-VPT-09100
*   RFQ-010:Vac-VEPT-0A100
    *   RFQ-010:Vac-VPT-0A100
